# Copyright (C) 2011 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PATH_BLOBS_TAPIOCA := vendor/sony/tapioca_ss/proprietary

# Prebuilt libraries that are needed to build open-source libraries
PRODUCT_COPY_FILES := \
    $(PATH_BLOBS_TAPIOCA)/lib/libaudioalsa.so:obj/lib/libaudioalsa.so \
    $(PATH_BLOBS_TAPIOCA)/bin/battery_charging:system/bin/battery_charging \
    $(PATH_BLOBS_TAPIOCA)/bin/bridgemgrd:system/bin/bridgemgrd \
    $(PATH_BLOBS_TAPIOCA)/bin/btld:system/bin/btld \
    $(PATH_BLOBS_TAPIOCA)/bin/btnvtool:system/bin/btnvtool \
    $(PATH_BLOBS_TAPIOCA)/bin/cnd:system/bin/cnd \
    $(PATH_BLOBS_TAPIOCA)/bin/ds_fmc_appd:system/bin/ds_fmc_appd \
    $(PATH_BLOBS_TAPIOCA)/bin/mm-pp-daemon:system/bin/mm-pp-daemon \
    $(PATH_BLOBS_TAPIOCA)/bin/netmgrd:system/bin/netmgrd \
    $(PATH_BLOBS_TAPIOCA)/bin/nvcustomizer:system/bin/nvcustomizer \
    $(PATH_BLOBS_TAPIOCA)/bin/port-bridge:system/bin/port-bridge \
    $(PATH_BLOBS_TAPIOCA)/bin/qmiproxy:system/bin/qmiproxy \
    $(PATH_BLOBS_TAPIOCA)/bin/qmuxd:system/bin/qmuxd \
    $(PATH_BLOBS_TAPIOCA)/bin/rild:system/bin/rild \
    $(PATH_BLOBS_TAPIOCA)/bin/startupflag:system/bin/startupflag \
    $(PATH_BLOBS_TAPIOCA)/bin/updatemiscta:system/bin/updatemiscta \
    $(PATH_BLOBS_TAPIOCA)/bin/wiperiface:system/bin/wiperiface \
    $(PATH_BLOBS_TAPIOCA)/etc/bcm4330/BCM4330B1_002.001.003.0750.0775.hcd:system/etc/bcm4330/BCM4330B1_002.001.003.0750.0775.hcd \
    $(PATH_BLOBS_TAPIOCA)/etc/bcm4330/bcm94330wlsdgbphone.txt:system/etc/bcm4330/bcm94330wlsdgbphone.txt \
    $(PATH_BLOBS_TAPIOCA)/etc/bcm4330/sdio-g-mfgtest-seqcmds.bin:system/etc/bcm4330/sdio-g-mfgtest-seqcmds.bin \
    $(PATH_BLOBS_TAPIOCA)/etc/bcm4330/sdio_g_pool_pno_pktfilter_keepalive_wapi_wme_idsup_idauth_apsta_aoe.bin:system/etc/bcm4330/sdio_g_pool_pno_pktfilter_keepalive_wapi_wme_idsup_idauth_apsta_aoe.bin \
    $(PATH_BLOBS_TAPIOCA)/etc/bcm4330/sdio_g_pool_pno_pktfilter_keepalive_wapi_wme_idsup_idauth_p2p_aoe.bin:system/etc/bcm4330/sdio_g_pool_pno_pktfilter_keepalive_wapi_wme_idsup_idauth_p2p_aoe.bin \
    $(PATH_BLOBS_TAPIOCA)/etc/bcm4330/sdio_g_pool_pno_pktfilter_keepalive_wapi_wme_idsup_idauth_sta_aoe.bin:system/etc/bcm4330/sdio_g_pool_pno_pktfilter_keepalive_wapi_wme_idsup_idauth_sta_aoe.bin \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/TAP_TMA340_0002.hex:system/etc/firmware/TAP_TMA340_0002.hex \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/TAP_TMA340_0005.hex:system/etc/firmware/TAP_TMA340_0005.hex \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/TAP_TMA340_0006.hex:system/etc/firmware/TAP_TMA340_0006.hex \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/TAP_TMA340_0007.hex:system/etc/firmware/TAP_TMA340_0007.hex \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/TAP_TMA340_0008.hex:system/etc/firmware/TAP_TMA340_0008.hex \
    $(PATH_BLOBS_TAPIOCA)/lib/hw/camera.msm7x27a.so:system/lib/hw/camera.msm7x27a.so \
    $(PATH_BLOBS_TAPIOCA)/lib/hw/gps.default.so:system/lib/hw/gps.default.so \
    $(PATH_BLOBS_TAPIOCA)/lib/hw/sensors.default.so:system/lib/hw/sensors.default.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libdivxdrmdecrypt.so:system/lib/libdivxdrmdecrypt.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAacDec.so:system/lib/libOmxAacDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAacEnc.so:system/lib/libOmxAacEnc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAdpcmDec.so:system/lib/libOmxAdpcmDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAmrDec.so:system/lib/libOmxAmrDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAmrEnc.so:system/lib/libOmxAmrEnc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAmrRtpDec.so:system/lib/libOmxAmrRtpDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxAmrwbDec.so:system/lib/libOmxAmrwbDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxEvrcDec.so:system/lib/libOmxEvrcDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxEvrcEnc.so:system/lib/libOmxEvrcEnc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxEvrcHwDec.so:system/lib/libOmxEvrcHwDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxH264Dec.so:system/lib/libOmxH264Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxIttiamVdec.so:system/lib/libOmxIttiamVdec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxMp3Dec.so:system/lib/libOmxMp3Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxMpeg4Dec.so:system/lib/libOmxMpeg4Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxOn2Dec.so:system/lib/libOmxOn2Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxQcelp13Dec.so:system/lib/libOmxQcelp13Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxQcelp13Enc.so:system/lib/libOmxQcelp13Enc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxQcelpHwDec.so:system/lib/libOmxQcelpHwDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxVidEnc.so:system/lib/libOmxVidEnc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxVp8Dec.so:system/lib/libOmxVp8Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxWmaDec.so:system/lib/libOmxWmaDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxWmvDec.so:system/lib/libOmxWmvDec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOmxrv9Dec.so:system/lib/libOmxrv9Dec.so \
    $(PATH_BLOBS_TAPIOCA)/lib/lib_get_huk.so:system/lib/lib_get_huk.so \
    $(PATH_BLOBS_TAPIOCA)/lib/lib_get_rooting_status.so:system/lib/lib_get_rooting_status.so \
    $(PATH_BLOBS_TAPIOCA)/lib/liballjoyn.so:system/lib/liballjoyn.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libaudioalsa.so:system/lib/libaudioalsa.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libauth.so:system/lib/libauth.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libcm.so:system/lib/libcm.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libcneapiclient.so:system/lib/libcneapiclient.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libcneqmiutils.so:system/lib/libcneqmiutils.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libcneutils.so:system/lib/libcneutils.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libcneutils.so:system/lib/libcneutils.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libcommondefs.so:system/lib/libcommondefs.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libdiag.so:system/lib/libdiag.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libdsi_netctrl.so:system/lib/libdsi_netctrl.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libdsm.so:system/lib/libdsm.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libdsutils.so:system/lib/libdsutils.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libfmradio.so:system/lib/libfmradio.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libfmradio.brcm-prop_rx.so:system/lib/libfmradio.brcm-prop_rx.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libgemini.so:system/lib/libgemini.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libgps.utils.so:system/lib/libgps.utils.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libidl.so:system/lib/libidl.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libloc_adapter.so:system/lib/libloc_adapter.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libloc_api-rpc-qc.so:system/lib/libloc_api-rpc-qc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libloc_eng.so:system/lib/libloc_eng.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libmiscta.so:system/lib/libmiscta.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libmm-abl-oem.so:system/lib/libmm-abl-oem.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libmm-abl.so:system/lib/libmm-abl.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libmm-adspsvc.so:system/lib/libmm-adspsvc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libmmipl.so:system/lib/libmmipl.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libmmjpeg.so:system/lib/libmmjpeg.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libnetmgr.so:system/lib/libnetmgr.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libnv.so:system/lib/libnv.so \
    $(PATH_BLOBS_TAPIOCA)/lib/liboem_rapi.so:system/lib/liboem_rapi.so \
    $(PATH_BLOBS_TAPIOCA)/lib/liboemcamera.so:system/lib/liboemcamera.so \
    $(PATH_BLOBS_TAPIOCA)/lib/liboncrpc.so:system/lib/liboncrpc.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libpbmlib.so:system/lib/libpbmlib.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libqdi.so:system/lib/libqdi.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libqdp.so:system/lib/libqdp.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libqmi.so:system/lib/libqmi.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libqmiservices.so:system/lib/libqmiservices.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libqueue.so:system/lib/libqueue.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libril-qc-1.so:system/lib/libril-qc-1.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libril-qc-qmi-1.so:system/lib/libril-qc-qmi-1.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libril-qcril-hook-oem.so:system/lib/libril-qcril-hook-oem.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libril.so:system/lib/libril.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libwms.so:system/lib/libwms.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libwmsts.so:system/lib/libwmsts.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libxml.so:system/lib/libxml.so

# QCOM Adreno
PRODUCT_COPY_FILES += \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/a225_pfp.fw:system/etc/firmware/a225_pfp.fw \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/a225_pm4.fw:system/etc/firmware/a225_pm4.fw \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/a225p5_pm4.fw:system/etc/firmware/a225p5_pm4.fw \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/leia_pfp_470.fw:system/etc/firmware/leia_pfp_470.fw \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/leia_pm4_470.fw:system/etc/firmware/leia_pm4_470.fw \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/yamato_pfp.fw:system/etc/firmware/yamato_pfp.fw \
    $(PATH_BLOBS_TAPIOCA)/etc/firmware/yamato_pm4.fw:system/etc/firmware/yamato_pm4.fw \
    $(PATH_BLOBS_TAPIOCA)/lib/egl/libGLESv2_adreno200.so:system/lib/egl/libGLESv2_adreno200.so \
    $(PATH_BLOBS_TAPIOCA)/lib/egl/eglsubAndroid.so:system/lib/egl/eglsubAndroid.so \
    $(PATH_BLOBS_TAPIOCA)/lib/egl/libGLESv2S3D_adreno200.so:system/lib/egl/libGLESv2S3D_adreno200.so \
    $(PATH_BLOBS_TAPIOCA)/lib/egl/libq3dtools_adreno200.so:system/lib/egl/libq3dtools_adreno200.so \
    $(PATH_BLOBS_TAPIOCA)/lib/egl/libEGL_adreno200.so:system/lib/egl/libEGL_adreno200.so \
    $(PATH_BLOBS_TAPIOCA)/lib/egl/libGLESv1_CM_adreno200.so:system/lib/egl/libGLESv1_CM_adreno200.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libc2d2_z180.so:system/lib/libc2d2_z180.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libsc-a2xx.so:system/lib/libsc-a2xx.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libOpenVG.so:system/lib/libOpenVG.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libC2D2.so:system/lib/libC2D2.so \
    $(PATH_BLOBS_TAPIOCA)/lib/libgsl.so:system/lib/libgsl.so
